#Google API#

Usage is fairly simple. I'll need to upload some images about how to set
up the Google Client through the dashboard. Once that's done, that client 
will be used for the SCA Application. 

To use gmail, create a Gmail Service like so: 

`$gmail_service = new SCA_Gmail_Service();`

This will build the OAuth in the background. If all things are set up
correctly, you'll be able to get services in. I still need to build a 
database layer for specific users. I have basic email fetching that 
you can use like so: 

`$gmail_service->get_message_by_id($messageId);`

This will return a message object with:

	Delivered_To
	Recieved
	Return_Path
	Date
	Subject
	From
	To
	Body

That are decoded form the base 64 encoding that get's passed back.

Nicer interfaces still need to be built for this. 

Sending mail is easy. Just do: 

`$gmail_service->send_message($mime);`

The $mime object is set up like so: 

`$message = new Outgoing_Mail_Mime;` 
`$message->to_name = "Alexander Kleinhans";` 
`$message->to_email = "zndr.k.94@gmail.com";` 
`$message->from_name = "SCA Nametest";` 
`$message->from_email = "scaapidemo@gmail.com";`
`$message->subject = "this is from the api";` 
`$message->body = "blah blah blah blah blah";` 

`$gmail_service->send_message($message->get_base64_mime());`

That's sending mail and getting mail. To get the $messageid for when
you want to pull mail, there's other things you need to do which I'm
going to make an easier way to do stuff. 

Configure stuff and make sure it's working through terminal first. 

I'll set up error handling later. 
