<?php
function expandHomeDirectory($path) {
	$homeDirectory = getenv('HOME');
	if (empty($homeDirectory)) {
	$homeDirectory = getenv("HOMEDRIVE") . getenv("HOMEPATH");
	}
	return str_replace('~', realpath($homeDirectory), $path);
}

/*------------------------------------------------
|	SCA_Google_Client
/*------------------------------------------------
| 	A necessary Google client object that has
|	to be authenticated for antying to work. 
|	Must be extended and validate here or 
|	else Service functionality will fail. 
*/

class SCA_Google_Client {
	// Autoloader path
	private $_autoloader_path; 

	// All pre-defined variables set up in constructor. 
	private $_client_validation_status; 
	private $_application_name; 
	private $_credentials_path; 
	private $_client_secret_path; 
	private $_scopes; 

	// Google Client object. 
	protected $_google_client; 
	private $_access_token; 
	private $_authorization_url; 
	private $_authorization_code; 

	// Check if client is valid
	protected $_is_valid; 

	public function __construct(){
		// initially set the client validation to false
		$this->_is_valid = false; 
		// @TODO load these from a database
		try {
			$dbh = new PDO("mysql:host=localhost;dbname=sca_google_api", "root", "nahannielenor"); 
			// Get the Client Credentials
			$sth = $dbh->prepare("SELECT * FROM sca_google_api_credentials"); 
			$sth->execute(); 
			$cred_res = $sth->fetch(PDO::FETCH_ASSOC); 
			$api_autoloader_path = $cred_res['api_autoloader_path']; 
			$application_name = $cred_res['application_name']; 
			$credentials_path = $cred_res['credentials_path']; 
			$client_secret_path = $cred_res['client_secret_path']; 
			// Set the Client scope
			$sth = $dbh->prepare("SELECT * FROM sca_gmail_scopes WHERE gmail_scope_name = 'gmail_full_access'"); 
			$sth->execute(); 
			$scope_res = $sth->fetch(PDO::FETCH_ASSOC); 
			$scope_url = $scope_res['gmail_scope_url'] . "<br>"; 	
			// Set the Google credentials 		
			$this->_autoloader_path = $api_autoloader_path; 
			$this->_application_name = $application_name;
			$this->_credentials_path = $credentials_path;
			$this->_client_secret_path = $client_secret_path;
			$this->_scopes = $scope_url; 
			// Require the autoloader	
			require $this->_autoloader_path; 	
			$this->create_client(); 
		} catch (PDOException $pdoex) {
			echo $pdoex->getMessage(); 
		}	
	}	
	
	protected function return_valid_client_after_oauth(){
  		$credentialsPath = expandHomeDirectory($this->_credentials_path);
		$this->fetch_access_token($credentialsPath); 			
		$this->refresh_access_token($credentialsPath); 
		// if all goes well, make evident that htis 
		// client is valid. 
		$this->_is_valid = true; 
		return $this->_google_client;
	}
	
	private function create_client(){
		$this->_google_client = new Google_Client();
		$this->_google_client->setApplicationName($this->_application_name);
		$this->_google_client->setScopes($this->_scopes);
		$this->_google_client->setAuthConfigFile($this->_client_secret_path);
		$this->_google_client->setAccessType('offline');
	}		
	

	private function fetch_access_token($credentialsPath){
		// Load previously authorized credentials from a file.
		// @TODO Load this from a database 
		if ($this->have_local_access_token($credentialsPath)){	
			$this->get_access_token_locally($credentialsPath); 
		} else {
			$this->get_access_token_from_user_auth(); 
		}
		$this->_google_client->setAccessToken($this->_access_token);
	}

	private function have_local_access_token($credentialsPath){
		// @TODO Check this from a database
		if (file_exists($credentialsPath)) {
			return true; 	
		}	 
		return false; 
	}

	private function get_access_token_locally($credentialsPath){
		// @TODO Load this from a database
		$this->_access_token = file_get_contents($credentialsPath);
	}
	
	private function get_access_token_from_user_auth(){
		// @TODO Get this from a redirect uri instead of mannually entering it
		// Request authorization from the user.
		$this->_authorization_url= $this->_google_client->createAuthUrl();
		printf("Open the following link in your browser:\n%s\n", $this->_authorization_url);
		print 'Enter verification code: ';
		$authorization_code= trim(fgets(STDIN));

		// Exchange authorization code for an access token.
		$this->_access_token = $this->_google_client->authenticate($this->_authorization_url);

		$this->save_access_token_locally(); 
	}

	private function save_access_token_locally(){
		// @TODO Load this to a database
		// Store the credentials to disk.
		if(!file_exists(dirname($credentialsPath))) {
			mkdir(dirname($credentialsPath), 0700, true);
		}
		file_put_contents($credentialsPath, $this->access_token);
		printf("Credentials saved to %s\n", $credentialsPath);
	}

	private function refresh_access_token($credentialsPath){
		// Refresh the token if it's expired.
		if ($this->_google_client->isAccessTokenExpired()) {
			$this->_google_client->refreshToken($this->_google_client->getRefreshToken());
			file_put_contents($credentialsPath, $this->_google_client->getAccessToken());
		}
	}

} 

$test = new SCA_Google_Client();
echo "OKAY"; 
