<?php



/* --------------------------------------------------
  |   Database Exception Codes for PHP
  |---------------------------------------------------
  |
  |   PHP expects errors to be number a lot of the
  |   time, so this will allow it to be passed as a
  |   code for handling, along side any message
  |
  |--------------------------------------------------- */

interface DatabaseHandle_Exception {
    /* ------------------------------------------------
      |   Database Initialization Errors
      |----------------------------------------------- */

    const EXCEPTION_CODE_UNFOUND_DATABASE_CREDENTIALS = 10;
    const EXCEPTION_CODE_PDO_OBJECT_NOT_INITIALIZED = 11;
    /* ------------------------------------------------
      |   Database Execution Errors
      |----------------------------------------------- */
    const EXCEPTION_CODE_INVALID_SELECT_ARGUMENT = 1;
    const EXCEPTION_CODE_INVALID_INSERT_ARGUMENT = 2;
    const EXCEPTION_CODE_INVALID_UPDATE_ARGUMENT = 3;
    const EXCEPTION_CODE_INVALID_DELETE_ARGUMENT = 4;
    const EXCEPTION_CODE_UNABLE_TO_QUERY = 6;
    const EXCEPTION_CODE_NON_OBJECT_PARAMETER = 7;

}

/* --------------------------------------------------
  |   Database Handle Interface
  |---------------------------------------------------
  |
  |   These are the public interface functions used
  |   in the actual update calls. All other error
  |   handling and implementation is in Worker class
  |
  |--------------------------------------------------- */

interface DatabaseHandle {

    function echo_test();

    function pdo_select($table, $columns, $where_array);

    function pdo_insert($table, $columns_key_array, $columns_value_array);

    function pdo_update($table, $columns, $where_array);

    function pdo_delete($table, $columns, $where_array);

    function pdo_fetch($query_result, $obj);

    function pdo_fetch_object($query_result);

    function pdo_fetch_array($querry_result);

    function pdo_fetch_assoc($query_result);

    function pdo_num_rows($pdo_query);
}

/* --------------------------------------------------
  |   Database Handle Worker
  |---------------------------------------------------
  |
  |   All error handling and exceptions are thrown
  |   here. Exceptions are caught in the director
  |   which expects values returned from here. All
  |   functions here are either protected or private.
  |
  |------------------------------------------------- */

class MySQL_DatabaseHandle_Worker implements DatabaseHandle_Exception {
    /* ----------------------------------------------
      |   INITIALIZE DATABASE HANDLE
      |-----------------------------------------------
      |   Credentials here protected and accessed in
      |   the director class (that this is worker of)
      |--------------------------------------------- */

    protected $_pdo;
    protected $_db_host;
    protected $_db_user;
    protected $_db_pass;
    protected $_db_name;
    protected $_db_port;
    protected $_db_type;

    const CREDS_INI = "connection.ini";

    protected $_pdo_attributes = array(
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
    );

    /* ----------------------------------------------
      |   GET CREDENTIALS LIKE USERNAME AND HOST
      |-----------------------------------------------
      |   Get username, password, database, host, etc.
      |   Files should be located in "connection.ini"
      |--------------------------------------------- */

    protected function get_and_set_credentials() {
        $database_credentials_filepath = DATA . 'database' . DIRECTORY_SEPARATOR . self::CREDS_INI;
        if (file_exists($database_credentials_filepath)) {
            $database_credentials_array = parse_ini_file($database_credentials_filepath);
            $this->_db_host = $database_credentials_array["db_host"];
            $this->_db_user = $database_credentials_array["db_user"];
            $this->_db_pass = $database_credentials_array["db_pass"];
            $this->_db_name = $database_credentials_array["db_name"];
            $this->_db_port = $database_credentials_array["db_port"];
            $this->_db_type = $database_credentials_array["db_type"];
        } else {
            throw new OutOfRangeException("could not retrieve credentials at " . __LINE__ . " of " . __METHOD__, self::EXCEPTION_CODE_UNFOUND_DATABASE_CREDENTIALS);
            //die();
        }
    }

    /* ----------------------------------------------
      |   INITIALIZE PDO OBJECT
      |-----------------------------------------------
      |   Create the new PDO object. Uses credentials
      |   found in ini file as well as set attributes.
      |--------------------------------------------- */

    protected function initialize_pdo() {
        try {
            $this->_pdo = new PDO($this->_db_type . ':host=' . $this->_db_host . ';port=' . $this->_db_port . ';dbname=' . $this->_db_name, $this->_db_user, $this->_db_pass, $this->_pdo_attributes);
            if ($this->_pdo == null) {
                throw new OutOfBoundsException("could not initialize new PDO object at " . __LINE__ . " of " . __METHOD__, self::EXCEPTION_CODE_PDO_OBJECT_NOT_INITIALIZED);
                //die();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            //logerror('Could not instantiate database connection at ' . __LINE__ . ' of ' . __METHOD__ . PHP_EOL . 'Reason: ' . $e->getMessage() . PHP_EOL . '--> Stack trace: ' . PHP_EOL . print_r(debug_backtrace(), 1), 'alert');
        }
    }

/* ----------------------------------------------
|   MySQL [SELECT] Worker Functions
|-----------------------------------------------
|   Worker functions for SELECT functionality.
|   Throws errors caught fby director.
|--------------------------------------------- */

    /* ------------------------------------------
      |   [SELECT] HELPER FUNCTION
      |   Generates a " WHERE " string for select query
      |----------------------------------------- */

    private function generate_where_string($where_array) {
        // generate a where string from array like " WHERE something = whatever "
        $where_string = "";
        foreach ($where_array as $key => $value) {
            $where_string .= " " . $key . "='" . $value . "' AND ";
        }
        $where_string = substr($where_string, 0, -5); // get's rid of last ' AND ' string;
        return $where_string;
    }

    /* ------------------------------------------
      |   Execute [SELECT] query
      |   Executes and returns an total select
      |   query.
      |----------------------------------------- */

    protected function generate_select_query($table, $columns = '*', $where_array = null) {
        // check for valid input parameters
        if (!is_string($table) || ($columns != '*' && !is_array($columns)) || (isset($where_array) && !is_array($where_array))) {
            throw new \InvalidArgumentException("invalid arguments passed at " . __LINE__ . " of " . __METHOD__, self::EXCEPTION_CODE_INVALID_SELECT_ARGUMENT);
            //die();
        } else {
            // generate a query string from parameters and return it
            $generated_query = "SELECT " . $columns . " FROM " . $table;
            if ($where_array == null) {
                return $generated_query;
            }
            $generated_query .= " WHERE " . $this->generate_where_string($where_array);
            return $generated_query;
        }
    }

    /* ----------------------------------------------
      |   MySQL [INSERT] Worker Functions
      |-----------------------------------------------
      |   Worker functions for INSERT functionality.
      |   Throws errors caught by director.
      |--------------------------------------------- */

    /* ------------------------------------------
      |   [INSERT] HELPER FUNCTION
      |   Generates a string to specify columns
      |----------------------------------------- */

    private function generate_columns_value_string($columns_value_array) {   // this function is used instead of implode so that the result
        // from an array is something like:
        //          'first','second','third'    <---- this function's result
        // instead of:
        //          first,second,third          <---- implodes() result
        $columns_value_string = "";
        foreach ($columns_value_array as $value) {
            $columns_value_string .= "'" . $value . "',";
        }
        $columns_value_string = substr($columns_value_string, 0, -1); // take off the last comma
        return $columns_value_string;
    }

    /* ------------------------------------------
      |   Execute [INSERT] query
      |   Executes and returns a total insert
      |   query.
      |----------------------------------------- */

    protected function generate_insert_query($table, $columns_key_array, $columns_value_array) {
        if (!is_string($table) || !is_array($columns_key_array) || !is_array($columns_value_array) || count($columns_key_array) != count($columns_value_array)) {
            throw new \InvalidArgumentException("invalid arguments passed at " . __LINE__ . " of " . __METHOD__, self::EXCEPTION_CODE_INVALID_INSERT_ARGUMENT);
            //die();
        } else {
            // generate a query string form parameters and return it
            $generated_query = "INSERT INTO " . $table . " (" . implode(",", $columns_key_array) . ") VALUES (" . $this->generate_columns_value_string($columns_value_array) . ")";
            return $generated_query;
        }
    }

    /* ----------------------------------------------
      |   @TODO MySQL [UPDATE] Worker Functions
      |-----------------------------------------------
      |   Worker functions for UPDATE functionality.
      |   Throws errors caught by director.
      |--------------------------------------------- */

    // @TODO UPDATE Worker Functions
    protected function generate_update_query($table, $columns = '*', $where_array = null) {
        
    }

    /* ----------------------------------------------
      |   @TODO MySQL [DELETE] Worker Functions
      |-----------------------------------------------
      |   Worker functions for DELETE functionality.
      |   Throws errors caught by director.
      |--------------------------------------------- */

    // @TODO [DELETE] Worker Functions
    protected function generate_delete_query($table, $columns = '*', $where_array = null) {
        
    }

    /* ----------------------------------------------
      |   MySQL [FETCH] Worker Functions
      |-----------------------------------------------
      |   Worker functions for FETCH functionality.
      |   Throws errors caught by director.
      |--------------------------------------------- */

    /* ------------------------------------------
      |   [FETCH] HELPER FUNCTION
      |   Returns PDO Object or Assoc depending
      |   on user requested spec.
      |----------------------------------------- */

    protected function fetch_from_query_result($query_result, $obj) {
        if (is_object($query_result)) {
            return $obj ? $query_result->fetch(PDO::FETCH_OBJ) : $query_result->fetch(PDO::FETCH_ASSOC);
        } else {
            throw new \InvalidArgumentException("non object paramater passed at " . __LINE__ . " of " . __METHOD__, self::EXCEPTION_CODE_NON_OBJECT_PARAMETER);
            //die();
        }
    }

    /* ------------------------------------------
      |   [FETCH OBJECT] HELPER FUNCTION
      |   Returns PDO Object
      |----------------------------------------- */

    protected function fetch_object_from_query_result($query_result) {
        if (is_object($query_result)) {
            return $query_result->fetch(PDO::FETCH_OBJ);
        } else {
            throw new \InvalidArgumentException("non object paramater passed at " . __LINE__ . " of " . __METHOD__, self::EXCEPTION_CODE_NON_OBJECT_PARAMETER);
            //die();
        }
    }

    /* ------------------------------------------
      |   [FETCH ASSOC] HELPER FUNCTION
      |   Returns PDO Assoc
      |----------------------------------------- */

    protected function fetch_assoc_from_query_result($query_result) {
        if (is_object($query_result)) {
            return $query_result->fetch(PDO::FETCH_ASSOC);
        } else {
            throw new \InvalidArgumentException("non object paramater passed at " . __LINE__ . " of " . __METHOD__, self::EXCEPTION_CODE_NON_OBJECT_PARAMETER);
            //die();
        }
    }

    /* ------------------------------------------
      |   [FETCH ARRAY] HELPER FUNCTION
      |   Returns PDO Array
      |----------------------------------------- */

    protected function fetch_array_from_query_result($query_result) {
        if (is_object($query_result)) {
            return $query_result->fetchAll();
        } else {
            throw new \InvalidArgumentException("non object paramater passed at " . __LINE__ . " of " . __METHOD__, self::EXCEPTION_CODE_NON_OBJECT_PARAMETER);
            //die();
        }
    }

    /* ----------------------------------------------
      |   RETURN NUM ROWS OF PDO OBJECT
      |-----------------------------------------------
      |   Returns number of rows of a PDO object.
      |   Checks the object type of the result first
      |   and throws a new error if result is not
      |   of a correct type.
      |--------------------------------------------- */

    protected function get_num_rows($query_result) {
        if (is_object($query_result)) {
            return $query_result->rowCount();
        } else {
            throw new \InvalidArgumentException("non object parameter passed at " . __LINE__ . " of " . __METHOD__, self::EXCEPTION_CODE_NON_OBJECT_PARAMETER);
            //die();
        }
    }

}

/* --------------------------------------------------
  |   Database Handle Director
  |---------------------------------------------------
  |
  |   Implements all functions of the DatabaseHandle
  |   interface function. Catches exceptions caught by
  |   worker for debugging.
  |
  |------------------------------------------------- */

class MySQL_DatabaseHandle extends MySQL_DatabaseHandle_Worker implements DatabaseHandle, DatabaseHandle_Exception {
    /* ----------------------------------------------
      |   MySQL DatabaseHandle Constructor
      |-----------------------------------------------
      |   Gets credentials from const ini file, then
      |   creates object.
      |--------------------------------------------- */

    public function echo_test() {
        echo "TESTING FROM DATABASE HANDLE";
    }

    public function __construct($debug_message = '') {
        echo $debug_message;
        try {
            $this->get_and_set_credentials();
            $this->initialize_pdo();
        } catch (Exception $e) {
            echo $e->getMessage();
            logerror('Database connection could not be instantiated at' . $e->getLine() . ' of ' . $e->getFile() . ': ' . $e->getMessage(), 'alert');
        }
    }

    /* ----------------------------------------------
      |   MySQL DatabaseHandle [SELECT]
      |-----------------------------------------------
      |   Returns result from a specified set of
      |   parameters for a select query.
      |--------------------------------------------- */

    public function pdo_select($table, $columns = '*', $where_array = null) {
        try {
            $query_string = $this->generate_select_query($table, $columns, $where_array);
            $result = $this->_pdo->query($query_string);
            return $result;
        } catch (Exception $e) {
            logerror('Query error at' . __LINE__ . ' of ' . __METHOD__ . ': ' . $e->getMessage());
        }
    }

    /* ----------------------------------------------
      |   MySQL DatabaseHandle [INSERT]
      |-----------------------------------------------
      |   Returns result from a specified set of
      |   parameters for an insert query.
      |--------------------------------------------- */

    public function pdo_insert($table, $column_key_array, $column_value_array) {
        try {
            $query_string = $this->generate_insert_query($table, $column_key_array, $column_value_array);
            $result = $this->_pdo->query($query_string);
            return $result;
        } catch (\InvalidArgumentException $e) {
            logerror('Query error at' . __LINE__ . ' of ' . __METHOD__ . ': ' . $e->getMessage());
        }
    }

    public function pdo_update($table, $columns = '*', $where_array = null) {
        // @TODO I haven't seen any UPDATE functions used in YWAM yet, so this is for later
    }

    public function pdo_delete($table, $columns = '*', $where_array = null) {
        // @TODO I haven't seen any DELETE functions used in YWAM yet, so this is for later too
    }

    /* ----------------------------------------------
      |   MySQL DatabaseHandle Fetch
      |-----------------------------------------------
      |   Returns result from a fetch of a PDO Object
      |   or Assoc depending on parameter spec.
      |--------------------------------------------- */

    public function pdo_fetch($query_result, $obj = False) {
        // return FETCH_ASSOC by default, otherwise FETCH_OBJ
        try {
            return $this->fetch_from_query_result($query_result, $obj);
        } catch (\InvalidArgumentException $e) {
            logerror('Query error at' . __LINE__ . ' of ' . __METHOD__ . ': ' . $e->getMessage());
        }
    }

    /* ----------------------------------------------
      |   MySQL DatabaseHandle Fetch Object
      |-----------------------------------------------
      |   Returns result from a fetch of a PDO Object
      |--------------------------------------------- */

    public function pdo_fetch_object($query_result) {
        // return FETCH_ASSOC by default, otherwise FETCH_OBJ
        try {
            return $this->fetch_object_from_query_result($query_result, $obj);
        } catch (\InvalidArgumentException $e) {
            logerror('Query error at' . __LINE__ . ' of ' . __METHOD__ . ': ' . $e->getMessage());
        }
    }

    /* ----------------------------------------------
      |   MySQL DatabaseHandle Fetch Array
      |-----------------------------------------------
      |   Returns result from a fetch of a PDO Array
      |--------------------------------------------- */

    public function pdo_fetch_array($query_result) {
        // return FETCH_ASSOC by default, otherwise FETCH_OBJ
        try {
            return $this->fetch_array_from_query_result($query_result);
        } catch (\InvalidArgumentException $e) {
            logerror('Query error at' . __LINE__ . ' of ' . __METHOD__ . ': ' . $e->getMessage());
        }
    }

    /* ----------------------------------------------
      |   MySQL DatabaseHandle Fetch Assoc
      |-----------------------------------------------
      |   Returns result from a fetch of a PDO Assoc
      |--------------------------------------------- */

    public function pdo_fetch_assoc($query_result) {
        // return FETCH_ASSOC by default, otherwise FETCH_OBJ
        try {
            return $this->fetch_assoc_from_query_result($query_result);
        } catch (\InvalidArgumentException $e) {
            logerror('Query error at' . __LINE__ . ' of ' . __METHOD__ . ': ' . $e->getMessage());
        }
    }

    /* ----------------------------------------------
      |   MySQL DatabaseHandle Num Rows
      |-----------------------------------------------
      |   Returns result from a number of rows for
      |   a PDO object, much like MySQL in YWAM.
      |--------------------------------------------- */

    public function pdo_num_rows($pdo_result) {
        try {
            return $this->get_num_rows($pdo_result);
        } catch (\InvalidArgumentException $e) {
            logerror('Query error at' . __LINE__ . ' of ' . __METHOD__ . ': ' . $e->getMessage());
        }
    }

    /* ----------------------------------------------
      |   Return Result of Regular Query String
      |-----------------------------------------------
      |   For ease of refactoring the exsisting
      |   functions in YWAM, using a streight query
      |   as generated by the original may be easier.
      |   This takes a query string to return result.
      |--------------------------------------------- */

    public function pdo_query($query_string) {
        // You're assuming $query_string is a valid query
        try {
            return $this->_pdo->query($query_string);
        } catch (Exception $e) {
            logerror('Query error at ' . __LINE__ . ' of ' . __METHOD__ . ': ' . $e->getMessage() . PHP_EOL . 'Stack Trace:' . PHP_EOL . print_r($e->getTrace(), 1));
        }
    }

}

/* ------------------------ EXAMPLE USAGE ------ */
//$dbh = new MySQL_DatabaseHandle();
//$result = $dbh->pdo_select('users', '*');
//if ($dbh->pdo_insert("users", array('username'), array('reloadtest'))){
//    echo "GOOD INSERT<br>";
//}
//echo "Reload the page and this will increment<br>";
//echo $dbh->pdo_num_rows($result) . "<br>";
