<?php

// This is the SCA Google API object. It needs to be validated
// and all other obejcts that use Google resources will extend 
// from this class. 
	
class SCA_Google_Client {

	// All pre-defined variables set up in constructor. 
	private $_client_validation_status; 
	private $_application_name; 
	private $_credentials_path; 
	private $_client_secret_path; 
	private $_scopes; 

	// Google Client object. 
	private $_google_client; 
	private $_access_token; 
	private $_authorization_url; 
	private $_authorization_code; 

	public function __construct(){
		// Set variable validation initially to flase. 
		$this->_client_validation_status = false; 		
		// You need to pull in the autoloader. 
		if (file_exists('google-api-php-client/src/Google/autoload.php')){
			require 'google-api-php-client/src/Google/autoload.php'; 
		} else {
			throw new Exception("unable to find autoloader"); 
		}
		// Set the application name variable. 
		$this->_application_name = 'SCA_API_Demo'; 
		// Set the credentials path variable. 
		$this->_credentials_path = '~/.credentials/gmail-api-quickstart.json';
		// Set secret path variable. 		
		if (file_exists('client_secret.json')){
			$this->_client_secret_path = 'client_secret.json';
		} else {
			throw new Exception("unable to find client secret"); 
		}
		// Set the scopes variable. 
  		$this->_scopes = implode(' ', array(Google_Service_Gmail::GMAIL_READONLY));
	}	
	
	public function create_client(){
		// Create the Google Client; 
		$this->_google_client = new Google_Client(); 
		// Set the client application name. 
		$this->_google_client->setApplicationName($this->_application_name);
		// Set the client scope(s).  
		$this->_google_client->setScopes($this->_scopes);
		// Set the client secret path. 	
		$this->_google_client->setAuthConfigFile($this->_client_secret_path);
		// Set the client access type ("offline for now"): 
		$this->_google_client->setAccessType('offline');
		echo "okay"; 
	}		
	
	public function return_valid_client_after_oauth(){
		// The majority of the OAuth is done at this state. 
		/* ALMOST DIRECTLY FORM THE QUICKSTART API */

		// Try to oad previously authorized credentials from a file.
		$supposedCredentialsPath = $this->_credentials_path;
		if (file_exists($supposedCredentialsPath )) {
			$this->_access_token = file_get_contents($supposedCredentialsPath);
		} else {
			// Request authorization from the user.
			$this->_authorization_url = $this->_google_client->createAuthUrl();
			printf("Open the following link in your browser:\n%s\n", $this->_authorization_url);
			print 'Enter verification code: ';
			$this->_authorization_code = trim(fgets(STDIN));

			// Exchange authorization code for an access token.
			$this->_access_token = $this->_google_client->authenticate($this->_autorization_url);

			// Store the credentials to disk.
			if(!file_exists(dirname($this->_credentials_path))) {
				mkdir(dirname($this->_credentials_path), 0700, true);
			}
			file_put_contents($this->_credentials_path, $this->_access_token);
			printf("Credentials saved to %s\n", $this->_credentials_path);
		}
		/*
		$this->fetch_access_token(); 

		$this->_google_client->setAccessToken($this->_access_token);
		
		$this->refresh_access_token(); 

		// Refresh the token if it's expired.
		*/
		if ($this->_google_client->isAccessTokenExpired()) {
			$this->_google_client->refreshToken($this->_google_client->getRefreshToken());
			file_put_contents($this->_credentials_path, $this->_google_client->getAccessToken());
		}
		echo "HERE"; 
		return $this->_google_client;
	}

	public function fetch_access_token(){
		// fetches the access token from a local resource or gets
		// authorization if necessary. 	
		if ($this->have_local_access_token){
			$this->get_access_token_locally(); 
		}	
		else {
			$this->get_access_token_from_user_auth(); 
			$this->save_access_token_locally(); 
		}
	}

	public function have_local_access_token(){
		// @TODO later this will check a database. 

		// returns true if we do, false if we don't. 	
		// later on will check database. 
		if (file_exists($this->_credentials_path)){
			return true; 
		}
		return false; 
	}

	public function get_access_token_locallly(){
		// @TODO later this will be pulled from a database. 

		$this->_access_token = file_get_contents($this->_credentials_path); 
	}
	
	public function get_access_token_from_user_auth(){
		// @TODO later this will get from a URL's $_GET paramater and
		// not have to be done mannually 

		$this->_authorization_url = $this->_google_client->createAuthUrl();
		printf("Open the following link in your browser:\n%s\n", $this->_authorization_url);
		print 'Enter verification code: ';
		$this->_authorization_code = trim(fgets(STDIN));

		// Exchange authorization code for an access token.
		$this->_access_token = $this->_google_client->authenticate($this->_autorization_url);
	}
	
	public function save_access_token_locally(){
		// @TODO later this will save to a database 

		// Store the credentials to disk.
		if(!file_exists(dirname($this->_credentials_path))) {
			mkdir(dirname($this->_credentials_path), 0700, true);
		}
		file_put_contents($this->_credentials_path, $this->_access_token);
		printf("Credentials saved to %s\n", $this->_credentials_path);
	}

	public function refresh_access_token(){
		// Refresh the token if it's expired.
		if ($this->_google_client->isAccessTokenExpired()) {
			$this->_google_client->refreshToken($this->_google_client->getRefreshToken());
			$this->_access_token = $this->_google_client->getAccessToken();
			$this->save_access_token_locally(); 
		}
	}
} 

try {
	$test = new SCA_Google_Client();  
} catch (Exception $e){
	echo $e->getMessage(); 
}
$test->create_client(); 
$test->return_valid_client_after_oauth(); 
echo "FINE"; 
?> 
