-- MySQL dump 10.13  Distrib 5.6.26, for osx10.9 (x86_64)
--
-- Host: localhost    Database: sca_google_api
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sca_gmail_scopes`
--

DROP TABLE IF EXISTS `sca_gmail_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sca_gmail_scopes` (
  `gmail_scope_name` varchar(255) DEFAULT NULL,
  `gmail_scope_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sca_gmail_scopes`
--

LOCK TABLES `sca_gmail_scopes` WRITE;
/*!40000 ALTER TABLE `sca_gmail_scopes` DISABLE KEYS */;
INSERT INTO `sca_gmail_scopes` VALUES ('gmail_read_only','https://www.googleapis.com/auth/gmail.readonly'),('gmail_read_only','https://www.googleapis.com/auth/gmail.readonly'),('gmail_compose_only','https://www.googleapis.com/auth/gmail.compose'),('gmail_send_only','https://www.googleapis.com/auth/gmail.send'),('gmail_insert_only','https://www.googleapis.com/auth/gmail.insert'),('gmail_labels_only','https://www.googleapis.com/auth/gmail.lables'),('gmail_modify_only','https://www.googleapis.com/auth/gmail.modify'),('gmail_full_access','https://mail.google.com/');
/*!40000 ALTER TABLE `sca_gmail_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sca_google_api_credentials`
--

DROP TABLE IF EXISTS `sca_google_api_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sca_google_api_credentials` (
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `api_autoloader_path` varchar(255) DEFAULT NULL,
  `application_name` varchar(255) DEFAULT NULL,
  `credentials_path` varchar(255) DEFAULT NULL,
  `client_secret_path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sca_google_api_credentials`
--

LOCK TABLES `sca_google_api_credentials` WRITE;
/*!40000 ALTER TABLE `sca_google_api_credentials` DISABLE KEYS */;
INSERT INTO `sca_google_api_credentials` VALUES ('2015-08-14 21:04:41','google-api-php-client/src/Google/autoload.php','SCA_API_Demo','~/.credentials/gmail-api-quickstart.json','client_secret.json');
/*!40000 ALTER TABLE `sca_google_api_credentials` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-17 16:37:48
