<?php
require_once('Google_Client_Class.php'); 
require_once('Outgoing_Mail_Mime.php'); 

/*------------------------------------------------
| 	Gmail_Fetched_Message	
/*------------------------------------------------
| 	A single fetched message with information
| 	such as body, header, date, recieved etc.
|	This is the object mainly used for 
| 	object retrieval because it's nicer. 
*/

class Gmail_Fetched_Message {
	public $delivered_to; 
	public $received; 
	public $return_path; 
	public $date; 
	public $subject; 
	public $from; 
	public $to; 

	public $body; 

	public function set_headers($headers_assoc){
		if (isset($headers_assoc["Delivered-To"])){
			$this->delivered_to = $headers_assoc["Delivered-To"];  
		}		
		if (isset($headers_assoc["Recieved"])){
			$this->recieved = $headers_assoc["Recieved"];  
		}		
		if (isset($headers_assoc["Return-Path"])){
			$this->return_path = $headers_assoc["Return-Path"];  
		}		
		if (isset($headers_assoc["Date"])){
			$this->date = $headers_assoc["Date"];  
		}		
		if (isset($headers_assoc["Subject"])){
			$this->subject = $headers_assoc["Subject"];  
		}		
		if (isset($headers_assoc["From"])){
			$this->from = $headers_assoc["From"];  
		}		
		if (isset($headers_assoc["To"])){
			$this->to = $headers_assoc["To"];  
		}		
	}

	public function set_body($rawData) {
		$sanitizedData = strtr($rawData, '-_', '+/'); 
		$this->body= base64_decode($sanitizedData); 
	}
}

/*------------------------------------------------
|	SCA_Gmail_Service	
/*------------------------------------------------
| 	A necessary Google client object that has
|	to be authenticated for antying to work. 
|	Must be extended and validate here or 
|	else Service functionality will fail. 
*/

final class SCA_Gmail_Service extends SCA_Google_Client {	
	// Google Service object. 
	public $_gmail_service; 

	public function __construct(){	
		parent::__construct(); 	
		// First, authenticate the Google Client
		$this->return_valid_client_after_oauth();
		$this->_gmail_service = new Google_Service_Gmail($this->_google_client);
	}

	public function do_google_service(){
		// Print the labels in the user's account.
		$user = 'me';
		$results = $this->_gmail_service->users_labels->listUsersLabels($user);
		if (count($results->getLabels()) == 0) {
			print "No labels found.\n";
		} else {
			print "Labels:\n";
			foreach ($results->getLabels() as $label) {
				printf("- %s\n", $label->getName());
			}
		}
	}

	public function send_message($mime){
		$msg = new Google_Service_Gmail_Message($mime); 
		$msg->setRaw($mime); 
		$userId = 'me';
		$msg = $this->_gmail_service->users_messages->send($userId, $msg);
		print 'Message with ID: ' . $msg->getId() . ' sent.';
	}
	
	public function get_mailbox_range_ids($start_index, $end_index, $box_name){
		$message = $this->_gmail_service->users_messages->listUsersMessages('me',['maxResults'=> 2, 'labelids' => 'INBOX']); 
		var_dump($messages); 
	}

	public function get_message_by_id($messageId){
		try {
			$message = $this->_gmail_service->users_messages->get("me", $messageId);
			$header_index_size = sizeof($message->getPayload()->getHeaders()); 
			echo $header_index_size;  
			$payload_headers = $message->getPayload()->getHeaders(); 

			// Pull in all headers info like subject and date and stuff
			$header_assoc = array(); 
			for ($header_index = 0; $header_index < $header_index_size; $header_index++){
				$name = $payload_headers[$header_index]->name; 
				$value = $payload_headers[$header_index]->value;
				$headers_assoc[$name] = $value; 	
			}

			// Set the headers
			$fetched_message = new Gmail_Fetched_Message(); 		
			$fetched_message->set_headers($headers_assoc); 
		
			// Get the body
			$parts = $message->getPayload()->getParts(); 
			$body_section = $parts[0]['body']; 
			$rawData = $body_section->data;
			$fetched_message->set_body($rawData); 		

			return $fetched_message; 
		} catch (Exception $e) {
			print 'An error occurred: ' . $e->getMessage();
		}
	}
}

$gmail_service = new SCA_Gmail_Service(); 
//var_dump($gmail_service->get_message_by_id("14f3ddfc396bc15d")); 
//$gmail_service->get_mailbox_range_ids(12,2,123); 

//$test->do_google_service(); 

/*
// SENDING A MESSAGE
$message = new Outgoing_Mail_Mime; 
$message->to_name = "Alexander Asshat Kleinhans"; 
$message->to_email = "zndr.k.94@gmail.com"; 
$message->from_name = "SCA Nametest"; 
$message->from_email = "scaapidemo@gmail.com";
$message->subject = "this is from the api"; 
$message->body = "blah blah blah blah blah"; 

$gmail_service->send_message($message->get_base64_mime());
*/
