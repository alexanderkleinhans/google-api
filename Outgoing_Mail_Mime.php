<?php 

/*------------------------------------------------
|	Outout_Mail_Mime	
/*------------------------------------------------
|	Creates a valid RFC 2822 base64 encoded 
| 	email mime if correct input data has been
| 	put in. Does not yet handle attatchments. 
|	Am able to set basic info, althogh maybe 
| 	some of this info should not be able to 
| 	be changed, i.e., if you send from email
| 	that is not you, an error will occur. 
*/

class Outgoing_Mail_Mime {
	// RFC standard
	public $to_name; 
	public $to_email; 
	public $from_name; 
	public $from_email;
	public $subject; 
	public $body; 

	public $text_mime; 
	public $base64_mime; 
	
	public function get_text_mime(){
		// Make sure all necessary email parts are set. 
		if (	!(isset($this->to_name)) ||	
			!(isset($this->to_email)) ||	
			!(isset($this->from_name)) ||	
			!(isset($this->from_email)) ||	
			!(isset($this->subject)) ||	
			!(isset($this->body))){
			throw new Exception ("unable to make mime, incomplete message parts"); 
		}
		// Create the mime 
		$this->text_mime = 
		"To: $this->to_name <$this->to_email>\n" .
		"From: $this->from_name <$this->from_email>\n".
		"Subject: $this->subject\n\n".
		"$this->body";
		// Return
		return $this->text_mime; 	
	} 

	public function get_base64_mime(){
		if (!isset($this->text_mime)){
			// If there is no set mime, set it
			$this->get_text_mime(); 	
		}
		// base64 encode it so it can be sent by email
		$this->base64_mime = rtrim(strtr(base64_encode($this->text_mime), '+/', '-_'), '=');
		// Return it
		return $this->base64_mime; 	
	}
}
